Name:           belcard
Version:        4.5.15
Release:        1%{?dist}
Summary:        Library to manipulate VCard format

License:        GPLv3
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  bctoolbox-devel
BuildRequires:  belr-devel
BuildRequires:  cmake
BuildRequires:  gcc-c++

%description
Belcard is a C++ library to manipulate VCard standard format.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1


%build
%cmake -DENABLE_STATIC=OFF
%cmake_build


%install
%cmake_install


%check
src="$(pwd)"
cd %{_vpath_builddir}/tester
ln -sfv %{buildroot}%{_datadir} .
./belcard_tester --resource-dir $src/tester


%{?ldconfig_scriptlets}


%files
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md
%{_bindir}/belcard-folder
%{_bindir}/belcard-parser
%{_bindir}/belcard-unfolder
%{_datadir}/belr/grammars/vcard_grammar
%{_libdir}/libbelcard.so.1

%files devel
%{_bindir}/belcard_tester
%{_datadir}/%{name}/
%{_datadir}/belcard_tester/
%{_includedir}/%{name}/
%{_libdir}/libbelcard.so


%changelog
